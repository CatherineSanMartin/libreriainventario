/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.dao.exceptions.NonexistentEntityException;
import root.dao.exceptions.PreexistingEntityException;
import root.persistence.entities.Inventariolibre;

/**
 *
 * @author cathe
 */
public class InventariolibreJpaController implements Serializable {

    public InventariolibreJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("inventario_PU");
    
    public InventariolibreJpaController() {
        
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Inventariolibre inventariolibre) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(inventariolibre);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findInventariolibre(inventariolibre.getId()) != null) {
                throw new PreexistingEntityException("Inventariolibre " + inventariolibre + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Inventariolibre inventariolibre) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            inventariolibre = em.merge(inventariolibre);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = inventariolibre.getId();
                if (findInventariolibre(id) == null) {
                    throw new NonexistentEntityException("The inventariolibre with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Inventariolibre inventariolibre;
            try {
                inventariolibre = em.getReference(Inventariolibre.class, id);
                inventariolibre.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The inventariolibre with id " + id + " no longer exists.", enfe);
            }
            em.remove(inventariolibre);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Inventariolibre> findInventariolibreEntities() {
        return findInventariolibreEntities(true, -1, -1);
    }

    public List<Inventariolibre> findInventariolibreEntities(int maxResults, int firstResult) {
        return findInventariolibreEntities(false, maxResults, firstResult);
    }

    private List<Inventariolibre> findInventariolibreEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Inventariolibre.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Inventariolibre findInventariolibre(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Inventariolibre.class, id);
        } finally {
            em.close();
        }
    }

    public int getInventariolibreCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Inventariolibre> rt = cq.from(Inventariolibre.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
