package root.services;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.dao.InventariolibreJpaController;
import root.persistence.entities.Inventariolibre;

@Path("/iventariolibre")
public class inventarioRest {

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("inventario_PU");
    EntityManager em;
    InventariolibreJpaController dao =new InventariolibreJpaController();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarTodo() {
       List<Inventariolibre> lista = dao.findInventariolibreEntities();
        return Response.ok(200).entity(lista).build();
    }
    
    
    @GET
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarId(@PathParam("idbuscar") String idbuscar) {

        em = emf.createEntityManager();
        Inventariolibre inventario = dao.findInventariolibre(idbuscar);
        return Response.ok(200).entity(inventario).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response nuevo(Inventariolibre Inventariolibrenuevo) throws Exception {

         InventariolibreJpaController inventario = new InventariolibreJpaController();
         inventario.create(Inventariolibrenuevo);
        
        return Response.ok("Producto Guardado").build();

    }

    @PUT
    public Response actualizar(Inventariolibre inventarioUpdate) throws Exception {
        String resultado = null;
        
        InventariolibreJpaController inventario = new InventariolibreJpaController();
         inventario.edit(inventarioUpdate);
        
        return Response.ok(inventarioUpdate).build();

      

    }
    
    @DELETE
    @Path("/{iddelete}")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON}) 
    public Response eliminaId(@PathParam("iddelete") String iddelete) throws Exception{
        
       InventariolibreJpaController inventario = new InventariolibreJpaController();
         inventario.destroy(iddelete);
        
        return Response.ok("Producto eliminado").build(); 
        
        
     
   }

    private Object String(String producto_Guardado) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
